package dev;

import java.util.Arrays;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class ExporterCSV implements ClientExporter{
    private static final String CSV_SEPARATOR = ";";

    @Override
    public void exporter(List<Client> ClientList) {
        try {
            BufferedWriter bw = new BufferedWriter(
                                new OutputStreamWriter(
                                new FileOutputStream("clientCSV.csv"), "UTF-8")
            );

            bw.append("name;email;phone\n");

            for (Client client : ClientList) {
                StringBuffer row = new StringBuffer();
                row.append(client.getName());
                row.append(CSV_SEPARATOR);
                row.append(client.getEmail());
                row.append(CSV_SEPARATOR);
                row.append(Arrays.toString(client.getPhone()));
                bw.write(row.toString());
            }

            bw.flush();
            bw.close();

            System.out.println("CSV archive writed...");
        } catch (UnsupportedEncodingException e) {
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }
}
