package dev;

import java.util.Arrays;

public class Client {
    private String name;
    private String email;
    private String[] phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String[] getPhone() {
        return phone;
    }

    public void setPhone(String[] phone) {
        this.phone = phone;
    }


    public Client(String name, String email, String... phone){
        this.setName(name);
        this.setEmail(email);
        this.setPhone(new String[]{Arrays.toString(phone)});
    }

    @Override
    public String toString() {
        return this.getName() +" "+ this.getEmail() +" "+ Arrays.toString(this.getPhone());
    }
}
