package dev;

import java.util.Arrays;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class ExporterHTML implements ClientExporter{
    @Override
    public void exporter(List<Client> ClientList) {
        try {
            BufferedWriter bw = new BufferedWriter(
                                new OutputStreamWriter(
                                new FileOutputStream("clientHTML.html"), "UTF-8")
            );

            bw.write("<html>\n");
            bw.write("\t<head>\n");
            bw.write("\t\t<title>Client List</title>\n");
            bw.write("\t</head>\n");
            bw.write("\t<body>\n");
            bw.write("\t\t<table border=1>\n");
            bw.write("\t\t\t<tr>\n");
            bw.write("\t\t\t\t<td>name</td>\n");
            bw.write("\t\t\t\t<td>email</td>\n");
            bw.write("\t\t\t\t<td>phones</td>\n");
            bw.write("\t\t\t</tr>\n");

            for (Client client : ClientList) {
                bw.write("\t\t\t<tr>\n");
                bw.write("\t\t\t\t<td>" + client.getName() + "</td>\n");
                bw.write("\t\t\t\t<td>" + client.getEmail() + "</td>\n");
                bw.write("\t\t\t\t<td>" + Arrays.toString(client.getPhone()) + "</td>\n");
                bw.write("\t\t\t</tr>\n");
            }

            bw.write("\t\t</table>\n");
            bw.write("\t</body>\n");
            bw.write("</html>\n");
            bw.flush();
            bw.close();

            System.out.println("HTML archive writed...");
        } catch (UnsupportedEncodingException e) {
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }
}
