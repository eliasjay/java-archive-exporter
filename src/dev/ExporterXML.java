package dev;

import java.util.Arrays;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class ExporterXML implements ClientExporter {
    @Override
    public void exporter(List<Client> ClientList) {
        try {
            BufferedWriter bw = new BufferedWriter(
                                new OutputStreamWriter(
                                new FileOutputStream("clientXML.xml"), "UTF-8")
            );

            bw.write("<clients>\n");

            for (Client client : ClientList) {
                bw.write("\t<client>\n");
                bw.write("\t\t<nome>" + client.getName() + "</nome>\n");
                bw.write("\t\t<email>" + client.getEmail() + "</email>\n");
                bw.write("\t\t<phones>\n");
                bw.write("\t\t\t<phone>" + Arrays.toString(client.getPhone()) + "</phone>\n");
                bw.write("\t\t</phones>\n");
                bw.write("\t</client>\n");
            }

            bw.write("</clients>\n");
            bw.flush();
            bw.close();

            System.out.println("XML archive writed...");
        } catch (UnsupportedEncodingException e) {
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }
}
