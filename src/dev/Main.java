package dev;

import java.util.ArrayList;
import java.util.List;
import static dev.Client.*;

public class Main {
    public static void main(String[] args) {

        List<Client> arrayClients = new ArrayList<>();

        Client jose = new Client("jose","jose@email.com", "(14) 99602-3246", "(14) 3488-1238");
        Client pedro = new Client("pedro","pedro@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client guilherme = new Client("guilherme","guilherme@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client igor = new Client("igor","igor@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client samuel = new Client("samuel","samuel@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client gabriel = new Client("gabriel","gabriel@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client micheli = new Client("micheli","micheli@email.com","(14) 99602-3246", "(14) 3488-1238");
        Client leticia = new Client("leticia","leticia@email.com","(14) 99602-3246", "(14) 3488-1238");

        arrayClients.add(jose);
        arrayClients.add(pedro);
        arrayClients.add(guilherme);
        arrayClients.add(igor);
        arrayClients.add(samuel);
        arrayClients.add(gabriel);
        arrayClients.add(micheli);
        arrayClients.add(leticia);

        ExporterCSV csv = new ExporterCSV();
        ExporterXML xml = new ExporterXML();
        ExporterHTML html = new ExporterHTML();

        csv.exporter(arrayClients);
        xml.exporter(arrayClients);
        html.exporter(arrayClients);
    }
}
